from dotenv import load_dotenv
from langchain.agents.agent_types import AgentType
from langchain_openai import ChatOpenAI
from langchain_experimental.agents.agent_toolkits import create_csv_agent

load_dotenv()

model = ChatOpenAI(temperature=0)

prompt = "Give me a deep dive analysis where I failed the most to achieve the sales target of 95%?"
input_data = "./input.csv"

agent = create_csv_agent(
    model, input_data, verbose=True, agent_type=AgentType.OPENAI_FUNCTIONS
)
agent.invoke(prompt)
