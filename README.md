# LangChain Simple Data Analytics

This project demonstrate the usage of LangChain in generating analysis based
on tabular data input and user prompt.

## Goal

The goal of this project is to generate analysis based on a combination of two types of input:
- A tabular data in form of CSV format:
    ```
    Province,City,Sales,Target,Promo,Budget
    DKI Jakarta,Jakarta Barat,1000,2000,100,200
    DKI Jakarta,Jakarta Selatan,2000,3500,100,200
    Jawa Barat,Bandung,1000,3000,100,200
    Jawa Barat,Depok,1000,5000,100,200
    ```
- Sentences of variable length as the prompt which describe the intended analysis:
    ```
    Give me a deep dive analysis where I failed the most to achieve the sales target of 95%?
    ```

The process of generating the final output should be carried out in manners that closely follow **Plan and Execute** strategy.

**The resulting output** should be delivered in form of analytical sentences that would satisfies the intention described in the prompt, e.g.:
```
DKI Jakarta failed significantly compared to others. The achievement is only 78%.
Within DKI Jakarta, there are 2 cities that failed to achieve the sales target over 95%.
```

## Pre-requisites

- [SerpAPI](https://serpapi.com) API key
- [OpenAI](https://platform.openai.com) API key

## Usage

1. Install the required packages
    ```shell
    pip install -r requirements.txt
    ```
2. Clone example environment file
    ```shell
    cp env.example .env
    ```
3. Fill your API keys in `.env` file
    ```shell
    OPENAI_API_KEY=<your OpenAI API key>
    SERPAPI_API_KEY=<your SerpAPI API key>
    ```
4. Run the script
    ```shell
    python {script name}.py
    ```

## Implementations

### Approach 1: Using pure `PlanAndExecute` scheme

This is the most basic approach that tries to digest all of the input as a single multiline string using `PlanAndExecute` agent.

```python
search = SerpAPIWrapper()
llm = OpenAI(temperature=0)
llm_math_chain = LLMMathChain.from_llm(llm=llm, verbose=True)

tools = [
    Tool(
        name="Search",
        func=search.run,
    ),
    Tool(
        name="Calculator",
        func=llm_math_chain.run,
    )
]

model = ChatOpenAI(temperature=0)
planner = load_chat_planner(model)
executor = load_agent_executor(model, tools, verbose=True)
agent = PlanAndExecute(planner=planner, executor=executor, verbose=True)

prompt = """
Given the following data:
Province,City,Sales,Target,Promo,Budget
DKI Jakarta,Jakarta Barat,1000,2000,100,200
DKI Jakarta,Jakarta Selatan,2000,3500,100,200
Jawa Barat,Bandung,1000,3000,100,200
Jawa Barat,Depok,1000,5000,100,200

Give me a deep dive analysis where I failed the most to achieve the sales target of 95%?
"""

agent.invoke(prompt)
```

#### Result

Initially, the approach showed promising results, indicated by the agent understanding that its first step is to calculate the percentage of sales on each city.
`````python
> Entering new PlanAndExecute chain...
steps=[Step(value='Calculate the target sales for each city by multiplying the target percentage (95%) with the target value.'), Step(value='Calculate the percentage of achieved sales for each city by dividing the actual sales by the target sales and multiplying by 100.'), Step(value='Calculate the difference between the achieved sales percentage and the target percentage for each city.'), Step(value='Identify the city with the highest negative difference, indicating the city where the sales target was missed the most.'), Step(value="Given the above steps taken, respond to the user's original question. ")]

> Entering new AgentExecutor chain...
Action:
{
  "action": "Calculator",
  "action_input": "95% * target value"
}

> Finished chain.
*****

Step: Calculate the target sales for each city by multiplying the target percentage (95%) with the target value.

Response: Action:
{
  "action": "Calculator",
  "action_input": "95% * target value"
}

> Entering new AgentExecutor chain...
Action:
```
{
  "action": "Calculator",
  "action_input": "actual sales / target sales * 100"
}
```

> Entering new LLMMathChain chain...
actual sales / target sales * 100```text
(actual_sales / target_sales) * 100
```
...numexpr.evaluate("(actual_sales / target_sales) * 100")...
`````

However, during the calculation step, the library failed to form a consistent dictionary to apply its formula
````python
> Entering new LLMMathChain chain...
actual sales / target sales * 100```text
(actual_sales / target_sales) * 100
```
...numexpr.evaluate("(actual_sales / target_sales) * 100")...
Traceback (most recent call last):
  File "/home/ak6/.pyenv/versions/dev/lib/python3.10/site-packages/numexpr/necompiler.py", line 762, in getArguments
    a = local_dict[name]
KeyError: 'actual_sales'

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/home/ak6/.pyenv/versions/dev/lib/python3.10/site-packages/langchain/chains/llm_math/base.py", line 89, in _evaluate_expression
    numexpr.evaluate(
  # (traceback)
  File "/home/ak6/.pyenv/versions/dev/lib/python3.10/site-packages/numexpr/necompiler.py", line 764, in getArguments
    a = global_dict[name]
KeyError: 'actual_sales'

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/home/ak6/personal/langchain-simple-data-analytics/main.py", line 47, in <module>
    agent.invoke(prompt)
  # (traceback)
  File "/home/ak6/.pyenv/versions/dev/lib/python3.10/site-packages/langchain/chains/llm_math/base.py", line 96, in _evaluate_expression
    raise ValueError(
ValueError: LLMMathChain._evaluate("
(actual_sales / target_sales) * 100
") raised error: 'actual_sales'. Please try again with a valid numerical expression
````

### Approach 2: Using CSV agent

This approach attempts to discover how LangChain handles CSV data by using `create_csv_agent`. The CSV data is stored in `input.csv` file and is read directly by the agent.

```python
from langchain.agents.agent_types import AgentType
from langchain_openai import ChatOpenAI
from langchain_experimental.agents.agent_toolkits import create_csv_agent

model = ChatOpenAI(temperature=0)

prompt = "Give me a deep dive analysis where I failed the most to achieve the sales target of 95%?"
input_data = "./input.csv"

agent = create_csv_agent(
    model, input_data, verbose=True, agent_type=AgentType.OPENAI_FUNCTIONS
)
agent.invoke(prompt)
```

#### Result

The agent generate solution in form of Python line of codes, suggesting user to utilize `pandas` to do numerical calculation on the data.

````python
> Entering new AgentExecutor chain...
To analyze where you failed the most to achieve the sales target of 95%, we can calculate the percentage of target achievement for each row in the dataframe and identify the rows where the achievement is below 95%. We can then group the data by Province and City to get a breakdown of the failures.

Here's the code to perform this analysis:

```python
# Calculate the percentage of target achievement for each row
df['Achievement'] = df['Sales'] / df['Target'] * 100

# Identify the rows where the achievement is below 95%
failures = df[df['Achievement'] < 95]

# Group the failures by Province and City
failure_analysis = failures.groupby(['Province', 'City']).size().reset_index(name='Failure Count')

# Sort the failure analysis by Failure Count in descending order
failure_analysis = failure_analysis.sort_values('Failure Count', ascending=False)

print(failure_analysis)
```

This code will add a new column 'Achievement' to the dataframe, which represents the percentage of target achievement for each row. It then filters the rows where the achievement is below 95% and groups them by Province and City. Finally, it sorts the failure analysis by the count of failures in descending order.

The resulting dataframe will show the Province, City, and the count of failures in each location, with the locations with the highest failure count at the top.

> Finished chain.
````

Following the suggestion will produce the following result:
```
  import pandas as pd
      Province             City  Failure Count
0  DKI Jakarta    Jakarta Barat              1
1  DKI Jakarta  Jakarta Selatan              1
2   Jawa Barat          Bandung              1
3   Jawa Barat            Depok              1
```

### Approach 3. Using CSV agent with zero-shot description flag

This time, a slight modification to approach 2 was made by using `ZERO_SHOT_REACT_DESCRIPTION` as the agent type

```python
from langchain.agents.agent_types import AgentType
from langchain_openai import ChatOpenAI
from langchain_experimental.agents.agent_toolkits import create_csv_agent

model = ChatOpenAI(temperature=0)

prompt = "Give me a deep dive analysis where I failed the most to achieve the sales target of 95%?"
input_data = "./input.csv"

agent = create_csv_agent(
    model, input_data, verbose=True, agent_type=AgentType.ZERO_SHOT_REACT_DESCRIPTION
)
agent.invoke(prompt)
```

#### Result

Upon execution it was discovered that the agent requires `python_repl_ast` tool in order to work.

````python
> Entering new AgentExecutor chain...
Thought: To find out where I failed the most to achieve the sales target of 95%, I need to calculate the percentage of sales achieved for each row and compare it to the target. Then I can find the row with the lowest percentage.

Action: Calculate the percentage of sales achieved for each row.

Action Input:
```python_repl_ast
df['Sales Percentage'] = df['Sales'] / df['Target'] * 100
df.head()
```

Observation: Calculate the percentage of sales achieved for each row. is not a valid tool, try one of [python_repl_ast].
Thought:Traceback (most recent call last):
  File "/home/ak6/.pyenv/versions/dev/lib/python3.10/site-packages/langchain/agents/agent.py", line 1125, in _iter_next_step
    output = self.agent.plan(
  # (traceback)
  File "/home/ak6/.pyenv/versions/dev/lib/python3.10/site-packages/langchain/agents/mrkl/output_parser.py", line 63, in parse
    raise OutputParserException(
langchain_core.exceptions.OutputParserException: Could not parse LLM output: `I need to use the `python_repl_ast` tool to calculate the percentage of sales achieved for each row.`

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/home/ak6/personal/langchain-simple-data-analytics/approach_2.py", line 16, in <module>
    agent.invoke(prompt)
  # (traceback)
  File "/home/ak6/.pyenv/versions/dev/lib/python3.10/site-packages/langchain/agents/agent.py", line 1136, in _iter_next_step
    raise ValueError(
ValueError: An output parsing error occurred. In order to pass this error back to the agent and have it try again, pass `handle_parsing_errors=True` to the AgentExecutor. This is the error: Could not parse LLM output: `I need to use the `python_repl_ast` tool to calculate the percentage of sales achieved for each row.`
````

This issue can be fixed by appending the following instruction into the prompt:
```python
prompt = prompt + " using tool python_repl_ast"
```

Afterward, the agent produced the following result:
````python
> Entering new AgentExecutor chain...
Thought: To find out where I failed the most to achieve the sales target of 95%, I need to calculate the percentage of sales achieved for each row and compare it with the target. Then I can find the row with the lowest percentage.

Action: python_repl_ast
Action Input: df['Sales_Percentage'] = df['Sales'] / df['Target'] * 100
Observation:
Thought:I have created a new column called 'Sales_Percentage' which contains the percentage of sales achieved for each row.

Action: python_repl_ast
Action Input: df['Sales_Percentage']
Observation: 0    50.000000
1    57.142857
2    33.333333
3    20.000000
Name: Sales_Percentage, dtype: float64
Thought:I can see that the 'Sales_Percentage' column has been successfully calculated for each row. Now I can find the row with the lowest percentage.

Action: python_repl_ast
Action Input: df.loc[df['Sales_Percentage'].idxmin()]
Observation: Province            Jawa Barat
City                     Depok
Sales                     1000
Target                    5000
Promo                      100
Budget                     200
Sales_Percentage          20.0
Name: 3, dtype: object
Thought:The row with the lowest sales percentage is the one where I failed the most to achieve the sales target of 95%.

Final Answer: The province is Jawa Barat, the city is Depok, and the sales percentage is 20.0%.

> Finished chain.
````

The final answer satisfies the intention of the prompt and therefore can be considered as the solution.

## Conclusion

Through trial and error, it was discovered that using a zero-shot CSV agent is the best approach to handle prompts attached with tabular data, as it can leverage the pandas library to perform calculations and produce concise answers. On the other hand, the use of SerpAPI and other "Plan and Execute" tools for these forms of input has proven to be counterproductive, as demonstrated in the earlier experiment.
