from dotenv import load_dotenv
from langchain.agents.tools import Tool
from langchain.agents.agent_types import AgentType
from langchain.chains import LLMMathChain
from langchain_openai import ChatOpenAI, OpenAI
from langchain_community.utilities import SerpAPIWrapper
from langchain_experimental.agents.agent_toolkits import create_csv_agent
from langchain_experimental.plan_and_execute import (
    PlanAndExecute,
    load_agent_executor,
    load_chat_planner,
)

load_dotenv()

search = SerpAPIWrapper()
llm = OpenAI(temperature=0)
llm_math_chain = LLMMathChain.from_llm(llm=llm, verbose=True)

tools = [
    Tool(
        name="Search",
        func=search.run,
        description="useful for when you need to answer questions about current events",
    ),
    Tool(
        name="Calculator",
        func=llm_math_chain.run,
        description="useful for when you need to answer questions about math",
    ),
]

model = ChatOpenAI(temperature=0)
planner = load_chat_planner(model)
executor = load_agent_executor(model, tools, verbose=True)
agent = PlanAndExecute(planner=planner, executor=executor, verbose=True)

prompt = """
Given the following data:
Province,City,Sales,Target,Promo,Budget
DKI Jakarta,Jakarta Barat,1000,2000,100,200
DKI Jakarta,Jakarta Selatan,2000,3500,100,200
Jawa Barat,Bandung,1000,3000,100,200
Jawa Barat,Depok,1000,5000,100,200

Give me a deep dive analysis where I failed the most to achieve the sales target of 95%?
"""

agent.invoke(prompt)
