import pandas as pd

df = pd.read_csv("./input.csv")

# Calculate the percentage of target achievement for each row
df["Achievement"] = df["Sales"] / df["Target"] * 100

# Identify the rows where the achievement is below 95%
failures = df[df["Achievement"] < 95]

# Group the failures by Province and City
failure_analysis = (
    failures.groupby(["Province", "City"]).size().reset_index(name="Failure Count")
)

# Sort the failure analysis by Failure Count in descending order
failure_analysis = failure_analysis.sort_values("Failure Count", ascending=False)

print(failure_analysis)
